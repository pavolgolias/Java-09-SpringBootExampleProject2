﻿## Spring - Example project - Users sign up / sign in with additional features (show/edit profile, forgot/reset password, email sending, etc.)
This project contains examples of some key features of Spring framework like MVC, Spring Data, Spring Security, AOP, Spring Boot Actuator and other areas. 
