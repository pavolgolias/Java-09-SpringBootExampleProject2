package sk.pgl.spring.entities;

import sk.pgl.spring.util.MyUtil;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "usr", indexes = {
        @Index(columnList = "email", unique = true),
        @Index(columnList = "forgotPasswordCode", unique = true)
})
public class User {
    public static final int EMAIL_MAX = 250;
    public static final int NAME_MAX = 50;
    public static final int PASSWORD_MAX = 30;
    public static final int RANDOM_CODE_LENGTH = 16;
    public static final String EMAIL_REGEX = "[A-Za-z0-9._%-+]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false, length = EMAIL_MAX)
    private String email;

    @Column(nullable = false, length = NAME_MAX)
    private String name;

    @Column(nullable = false)
    private String password;

    @ElementCollection(fetch = FetchType.EAGER)
    private Set<Role> roles = new HashSet<>();

    @Column(length = RANDOM_CODE_LENGTH)
    private String verificationCode;

    @Column(length = RANDOM_CODE_LENGTH)
    private String forgotPasswordCode;

    public User() {
    }

    public User(String email, String name, String password) {
        this.email = email;
        this.name = name;
        this.password = password;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public String getVerificationCode() {
        return verificationCode;
    }

    public void setVerificationCode(String verificationCode) {
        this.verificationCode = verificationCode;
    }

    public String getForgotPasswordCode() {
        return forgotPasswordCode;
    }

    public void setForgotPasswordCode(String forgotPasswordCode) {
        this.forgotPasswordCode = forgotPasswordCode;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", name='" + name + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

    public boolean isAdmin() {
        return roles.contains(Role.ADMIN);
    }

    public boolean isEditable() {
        User loggedIn = MyUtil.getSessionUser();
        return loggedIn != null && (loggedIn.isAdmin() || loggedIn.getId() == id);
    }

    public enum Role {
        UNVERIFIED, BLOCKED, ADMIN
    }
}
