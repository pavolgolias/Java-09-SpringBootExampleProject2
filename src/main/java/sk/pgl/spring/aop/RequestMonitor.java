package sk.pgl.spring.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class RequestMonitor {
    private static final Logger logger = LoggerFactory.getLogger(RequestMonitor.class);

    @Around("@annotation(org.springframework.web.bind.annotation.GetMapping), @annotation(org.springframework.web.bind.annotation.PostMapping)")
    public Object wrap(ProceedingJoinPoint joinPoint) throws Throwable {

        logger.info("Before controller method " + joinPoint.getSignature().getName() + ". Thread "
                + Thread.currentThread().getName());
        Object retVal = joinPoint.proceed();
        logger.info("Controller method " + joinPoint.getSignature().getName() + " execution successful.");

        return retVal;
    }
}
