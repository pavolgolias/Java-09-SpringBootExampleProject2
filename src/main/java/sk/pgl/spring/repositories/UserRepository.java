package sk.pgl.spring.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import sk.pgl.spring.entities.User;

public interface UserRepository extends JpaRepository<User, Long> {

    //@Query("select u from User u where u.email = ?1")
    //@Query(value = "SELECT * FROM USERS WHERE EMAIL_ADDRESS = ?1", nativeQuery = true)
    User findByEmail(String email);

    User findByForgotPasswordCode(String forgotPasswordCode);
}

// DOC: https://docs.spring.io/spring-data/jpa/docs/current/reference/html/
