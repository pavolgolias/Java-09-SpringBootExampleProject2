package sk.pgl.spring.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import sk.pgl.spring.dto.ForgotPasswordForm;
import sk.pgl.spring.dto.ResetPasswordForm;
import sk.pgl.spring.dto.SignupForm;
import sk.pgl.spring.services.UserService;
import sk.pgl.spring.util.MyUtil;
import sk.pgl.spring.validators.ForgotPasswordFormValidator;
import sk.pgl.spring.validators.ResetPasswordFormValidator;
import sk.pgl.spring.validators.SignupFormValidator;

import javax.validation.Valid;

@Controller
public class RootController {
    private static final Logger logger = LoggerFactory.getLogger(RootController.class);

    private UserService userService;
    private SignupFormValidator signupFormValidator;
    private ForgotPasswordFormValidator forgotPasswordFormValidator;
    private ResetPasswordFormValidator resetPasswordFormValidator;

    @Autowired
    public RootController(UserService userService,
            SignupFormValidator signupFormValidator,
            ForgotPasswordFormValidator forgotPasswordFormValidator,
            ResetPasswordFormValidator resetPasswordFormValidator) {
        this.userService = userService;
        this.signupFormValidator = signupFormValidator;
        this.forgotPasswordFormValidator = forgotPasswordFormValidator;
        this.resetPasswordFormValidator = resetPasswordFormValidator;
    }

    /*@RequestMapping("/")
    public String index() {
        return "home";
    }*/

    @InitBinder("signupForm")
    protected void initSignupBinder(WebDataBinder binder) {
        binder.setValidator(signupFormValidator);
    }

    @InitBinder("forgotPasswordForm")
    protected void initForgotPasswordFormValidator(WebDataBinder binder) {
        binder.setValidator(forgotPasswordFormValidator);
    }

    @InitBinder("resetPasswordForm")
    protected void initResetPasswordFormValidator(WebDataBinder binder) {
        binder.setValidator(resetPasswordFormValidator);
    }

    @GetMapping("/signup")
    public String index(Model model) {
        // is is the same like this --> model.addAttribute("signupForm", new SignupForm());
        model.addAttribute(new SignupForm());

        return "signup";
    }

    @PostMapping("/signup")
    public String signup(@ModelAttribute("signupForm") @Valid SignupForm signupForm,
            BindingResult result,
            RedirectAttributes redirectAttributes) {
        logger.info(signupForm.toString());

        if (result.hasErrors()) {
            return "signup";
        }

        userService.signup(signupForm);

        MyUtil.flash(redirectAttributes, "success", "signupSuccess");

        return "redirect:/";
    }

    @GetMapping("/forgot-password")
    public String forgotPassword(Model model) {
        model.addAttribute(new ForgotPasswordForm());
        return "forgot-password";
    }

    @PostMapping("/forgot-password")
    public String forgotPassword(@ModelAttribute("forgotPasswordForm") @Valid ForgotPasswordForm forgotPasswordForm,
            BindingResult result,
            RedirectAttributes redirectAttributes) {

        if (result.hasErrors())
            return "forgot-password";

        userService.forgotPassword(forgotPasswordForm);
        MyUtil.flash(redirectAttributes, "info", "checkMailResetPassword");

        return "redirect:/";
    }

    @GetMapping("/reset-password/{forgotPasswordCode}")
    public String resetPassword(@PathVariable("forgotPasswordCode") String forgotPasswordCode, Model model) {
        model.addAttribute(new ResetPasswordForm());
        return "reset-password";
    }

    @PostMapping("/reset-password/{forgotPasswordCode}")
    public String resetPassword(@PathVariable("forgotPasswordCode") String forgotPasswordCode,
            @ModelAttribute("resetPasswordForm") @Valid ResetPasswordForm resetPasswordForm,
            BindingResult result,
            RedirectAttributes redirectAttributes) {

        userService.resetPassword(forgotPasswordCode, resetPasswordForm, result);

        if (result.hasErrors())
            return "reset-password";

        MyUtil.flash(redirectAttributes, "success", "passwordChanged");

        return "redirect:/login";
    }
}
