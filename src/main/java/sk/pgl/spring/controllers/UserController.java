package sk.pgl.spring.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import sk.pgl.spring.dto.UserEditForm;
import sk.pgl.spring.entities.User;
import sk.pgl.spring.services.UserService;
import sk.pgl.spring.util.MyUtil;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
@RequestMapping("/users")
public class UserController {
    private static final Logger logger = LoggerFactory.getLogger(UserController.class);
    private UserService userService;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/{verificationCode}/verify")
    public String verify(@PathVariable("verificationCode") String verificationCode,
            RedirectAttributes redirectAttributes,
            HttpServletRequest httpServletRequest) throws ServletException {
        logger.warn("SOM TU 1");
        userService.verify(verificationCode);
        logger.warn("SOM TU 2");
        MyUtil.flash(redirectAttributes, "success", "verificationSuccess");
        logger.warn("SOM TU 3");
        httpServletRequest.logout();
        logger.warn("SOM TU 4");

        return "redirect:/";
    }

    @GetMapping("/{userId}")
    public String getById(@PathVariable("userId") long userId, Model model) {
        model.addAttribute(/*"user", */userService.findOne(userId));
        return "user";
    }

    @GetMapping("/{userId}/edit")
    public String edit(@PathVariable("userId") long userId, Model model) {
        User user = userService.findOne(userId);
        UserEditForm form = new UserEditForm();
        form.setName(user.getName());
        form.setRoles(user.getRoles());
        model.addAttribute(form);

        return "user-edit";
    }

    @PostMapping("/{userId}/edit")
    public String edit(@PathVariable("userId") long userId,
            @ModelAttribute("userEditForm") @Valid UserEditForm userEditForm,
            BindingResult result,
            RedirectAttributes redirectAttributes,
            HttpServletRequest request) throws ServletException {

        if (result.hasErrors()) {
            return "user-edit";
        }

        userService.update(userId, userEditForm);
        MyUtil.flash(redirectAttributes, "success", "editSuccessful");
        request.logout();

        return "redirect:/";
    }
}
