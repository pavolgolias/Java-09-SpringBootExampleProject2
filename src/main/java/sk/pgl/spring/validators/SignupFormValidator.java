package sk.pgl.spring.validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import sk.pgl.spring.dto.SignupForm;
import sk.pgl.spring.entities.User;
import sk.pgl.spring.repositories.UserRepository;

@Component
public class SignupFormValidator extends LocalValidatorFactoryBean {
    private UserRepository userRepository;

    @Autowired
    public SignupFormValidator(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.isAssignableFrom(SignupForm.class);
    }

    @Override
    public void validate(Object target, Errors errors, Object... validationHints) {
        super.validate(target, errors, validationHints);

        if (!errors.hasErrors()) {
            SignupForm signupForm = (SignupForm) target;
            User user = userRepository.findByEmail(signupForm.getEmail());
            if (user != null) {
                errors.rejectValue("email", "emailNotUnique");
            }
        }
    }
}
