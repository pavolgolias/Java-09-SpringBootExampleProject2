package sk.pgl.spring.validators;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import sk.pgl.spring.dto.ForgotPasswordForm;
import sk.pgl.spring.entities.User;
import sk.pgl.spring.repositories.UserRepository;

@Component
public class ForgotPasswordFormValidator extends LocalValidatorFactoryBean {
    private UserRepository userRepository;

    public ForgotPasswordFormValidator(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.isAssignableFrom(ForgotPasswordForm.class);
    }

    @Override
    public void validate(Object target, Errors errors, Object... validationHints) {
        super.validate(target, errors, validationHints);

        if (!errors.hasErrors()) {
            ForgotPasswordForm forgotPasswordForm = (ForgotPasswordForm) target;
            User user = userRepository.findByEmail(forgotPasswordForm.getEmail());

            if (user == null) {
                errors.rejectValue("email", "notFound");    // email is a field, notFound is message
            }
        }
    }
}
