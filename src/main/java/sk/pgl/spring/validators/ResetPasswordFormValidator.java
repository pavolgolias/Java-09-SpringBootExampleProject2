package sk.pgl.spring.validators;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import sk.pgl.spring.dto.ResetPasswordForm;

@Component
public class ResetPasswordFormValidator extends LocalValidatorFactoryBean {

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.isAssignableFrom(ResetPasswordForm.class);
    }

    @Override
    public void validate(Object target, Errors errors, Object... validationHints) {
        super.validate(target, errors, validationHints);

        if (!errors.hasErrors()) {
            ResetPasswordForm resetPasswordForm = (ResetPasswordForm) target;
            if (!resetPasswordForm.getPassword().equals(resetPasswordForm.getRetypePassword())) {
                errors.reject("passwordsDoNotMatch");   // we put only message, not field, so it will be shown as global message
            }
        }
    }
}
