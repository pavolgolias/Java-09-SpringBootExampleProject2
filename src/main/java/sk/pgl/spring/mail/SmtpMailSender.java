package sk.pgl.spring.mail;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

public class SmtpMailSender implements MailSender {
    private static final Logger logger = LoggerFactory.getLogger(SmtpMailSender.class);

    private JavaMailSender javaMailSender;

    public void setJavaMailSender(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    @Override
    @Async
    public void send(String to, String subject, String body) throws MessagingException {
        logger.info("SmtpMailSender // Thread " + Thread.currentThread().getName());

        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper helper;

        helper = new MimeMessageHelper(message, true); // true indicates multipart message
        helper.setSubject(subject);
        helper.setTo(to);
        helper.setText(body, true); // true indicates html
        // continue using helper object for more functionalities like adding attachments, etc.

        //javaMailSender.send(message); // TODO call this after providing correct username/password credentials
        logger.info("--- SmtpMailSender--- ");
        logger.info("Sending mail to " + to);
        logger.info("Subject: " + subject);
        logger.info("Body: " + body);
    }
}
