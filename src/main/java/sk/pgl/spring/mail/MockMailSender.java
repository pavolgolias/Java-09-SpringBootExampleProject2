package sk.pgl.spring.mail;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;

public class MockMailSender implements MailSender {

    private static final Logger logger = LoggerFactory.getLogger(MockMailSender.class);

    @Override
    @Async
    public void send(String to, String subject, String body) {
        logger.info("MockMailSender // Thread " + Thread.currentThread().getName());

        logger.info("Sending mail to " + to);
        logger.info("Subject: " + subject);
        logger.info("Body: " + body);
    }
}
