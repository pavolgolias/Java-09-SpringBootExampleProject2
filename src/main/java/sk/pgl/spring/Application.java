package sk.pgl.spring;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.util.Arrays;

@SpringBootApplication
@EnableTransactionManagement
@EnableAsync
public class Application {
    private static final Logger logger = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(Application.class);

        logger.info("Beans in application context:");
        String[] beanNames = context.getBeanDefinitionNames();
        Arrays.sort(beanNames);

        for (String beanName : beanNames) {
            logger.info(beanName);
        }
    }
}

// Spring-Loaded: https://github.com/spring-projects/spring-loaded
// Spring Boot Actuator: https://github.com/spring-projects/spring-boot/tree/master/spring-boot-actuator

// another example of solution: https://bitbucket.org/glaubernespoli/spring-tutorial
// http://stackoverflow.com/questions/34114681/when-spring-boot-application-running-by-intelij-idea-views-cant-be-resolved

// Spring lemon: https://github.com/naturalprogrammer/spring-lemon
