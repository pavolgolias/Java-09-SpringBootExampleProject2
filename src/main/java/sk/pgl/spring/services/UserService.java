package sk.pgl.spring.services;

import org.springframework.validation.BindingResult;
import sk.pgl.spring.dto.ForgotPasswordForm;
import sk.pgl.spring.dto.ResetPasswordForm;
import sk.pgl.spring.dto.SignupForm;
import sk.pgl.spring.dto.UserEditForm;
import sk.pgl.spring.entities.User;

public interface UserService {
    void signup(SignupForm signupForm);

    void verify(String verificationCode);

    void forgotPassword(ForgotPasswordForm forgotPasswordForm);

    void resetPassword(String forgotPasswordCode, ResetPasswordForm resetPasswordForm, BindingResult result);

    User findOne(long userId);

    void update(long userId, UserEditForm userEditForm);
}


