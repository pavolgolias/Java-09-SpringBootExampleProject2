package sk.pgl.spring.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import sk.pgl.spring.dto.UserDetailsImpl;
import sk.pgl.spring.entities.User;

import java.util.Locale;

@Component
public class MyUtil {
    private static final String DEV_PROFILE_NAME = "dev";

    private static MessageSource messageSource;
    private static String hostAndPort;
    private static String activeProfiles;

    @Autowired
    public MyUtil(MessageSource messageSource) {
        MyUtil.messageSource = messageSource;
    }

    public static String hostUrl() {
        return (isDevelopmentProfile() ? "http://" : "https://") + hostAndPort;
    }

    public static boolean isDevelopmentProfile() {
        return activeProfiles.equals(DEV_PROFILE_NAME);
    }

    public static void flash(RedirectAttributes redirectAttributes, String kind, String messageKey) {
        redirectAttributes.addFlashAttribute("flashKind", kind);
        redirectAttributes.addFlashAttribute("flashMessage", MyUtil.getMessage(messageKey));
    }

    public static String getMessage(String messageKey, Object... args) {
        return messageSource.getMessage(messageKey, args, Locale.getDefault());
    }

    public static void validate(boolean valid, String messageContent, Object... args) {
        if (!valid) {
            throw new RuntimeException(getMessage(messageContent, args));
        }
    }

    public static User getSessionUser() {
        UserDetailsImpl auth = getAuth();
        return auth == null ? null : auth.getUser();
    }

    public static UserDetailsImpl getAuth() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication != null) {
            Object principal = authentication.getPrincipal();
            if (principal instanceof UserDetailsImpl) {
                return (UserDetailsImpl) principal;
            }
        }

        return null;
    }

    @Value("${hostAndPort}")
    public void setHostAndPort(String hostAndPort) {
        MyUtil.hostAndPort = hostAndPort;
    }

    @Value("${spring.profiles.active}")
    public void setActiveProfiles(String activeProfiles) {
        MyUtil.activeProfiles = activeProfiles;
    }
}
