package sk.pgl.spring.dto;

import sk.pgl.spring.entities.User;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class ResetPasswordForm {
    @NotNull
    @Size(min = 1, max = User.PASSWORD_MAX, message = "{passwordSizeError}")
    private String password = "";

    @NotNull
    @Size(min = 1, max = User.PASSWORD_MAX, message = "{passwordSizeError}")
    private String retypePassword = "";

    public ResetPasswordForm() {
    }

    public ResetPasswordForm(String password, String retypePassword) {
        this.password = password;
        this.retypePassword = retypePassword;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRetypePassword() {
        return retypePassword;
    }

    public void setRetypePassword(String retypePassword) {
        this.retypePassword = retypePassword;
    }

    @Override
    public String toString() {
        return "ResetPasswordForm{" +
                "password='" + password + '\'' +
                ", retypePassword='" + retypePassword + '\'' +
                '}';
    }
}
