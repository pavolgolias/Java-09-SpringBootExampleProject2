package sk.pgl.spring.dto;

import sk.pgl.spring.entities.User;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class ForgotPasswordForm {
    @NotNull
    @Size(min = 1, max = User.EMAIL_MAX, message = "{emailSizeError}")
    @Pattern(regexp = User.EMAIL_REGEX, message = "{emailPatternError}")
    private String email = "";

    public ForgotPasswordForm() {
    }

    public ForgotPasswordForm(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "ForgotPasswordForm{" +
                "email='" + email + '\'' +
                '}';
    }
}
