package sk.pgl.spring.dto;

import sk.pgl.spring.entities.User;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

public class UserEditForm {
    @NotNull
    @Size(min = 1, max = User.NAME_MAX, message = "{fieldSizeError}")
    private String name = "";

    private Set<User.Role> roles;

    public UserEditForm() {
    }

    public UserEditForm(String name, Set<User.Role> roles) {
        this.name = name;
        this.roles = roles;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<User.Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<User.Role> roles) {
        this.roles = roles;
    }

    @Override
    public String toString() {
        return "UserEditForm{" +
                "name='" + name + '\'' +
                ", roles=" + roles +
                '}';
    }
}
